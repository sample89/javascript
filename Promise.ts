/**
 * Promise 使用示例
 */
function executor(resolve, reject) {
    let rand = Math.random();
    console.log(1)
    console.log(rand)
    if (rand > 0.5)
        resolve()
    else
        reject()
}

var p0 = new Promise(executor);

var p1 = p0.then((value) => {
    console.log("succeed-1")
    return new Promise(executor)
})

var p3 = p1.then((value) => {
    console.log("succeed-2")
    return new Promise(executor)
})

var p4 = p3.then((value) => {
    console.log("succeed-3")
    return new Promise(executor)
})

p4.catch((error) => {
    console.log("error")
})
console.log(2)

/**
 * 问题一：Promise 为何要采用微任务？
 * 问题二：Promise 如何实现返回值穿透的？
 * 问题三：Promise 出错后是怎么通过冒泡传递给捕获异常那个函数？
 */
function Bromise(executor) {
    var onResolve_ = null;
    var onReject_ = null;

    //模拟实现 resolve 和 then，暂不支持 reject
    this.then = function (onResolve, onReject) {
        onResolve_ = onResolve
    };

    function resolve(value) {
        setTimeout(()=>{
            onResolve_(value)
        },0)
    }

    executor(resolve, null);
}

class Promise {
    constructor(fn) {
        this.state = Promise.PENDING;
        this.value = undefined;
        this.reason = null;
        this.onFulfilledCallbacks = [];
        this.onRejectedCallbacks = [];
        fn(this.resolve.bind(this), this.reject.bind(this));
    }

    then(onFulfilled, onRejected) {
        if (this.state === Promise.FULFILLED) {
            onFulfilled(this.value);
        } else if (this.state === Promise.REJECTED) {
            onRejected(this.reason);
        } else {
            this.onFulfilledCallbacks.push(onFulfilled);
            this.onRejectedCallbacks.push(onRejected);
        }
        return this;
    }

    resolve(value) {
        this.state = Promise.FULFILLED;
        this.value = value;
        if (value.constructor === this.constructor) {
            value.onFulfilledCallbacks = [...this.onFulfilledCallbacks];
            value.onRejectedCallbacks = [...this.onRejectedCallbacks];
        } else {
            this.onFulfilledCallbacks.forEach((item) => {
                if (typeof item === 'function') {
                    item(value);
                }
            });
        }
    }

    reject(reason) {
        this.state = Promise.REJECTED;
        this.reason = reason;
        this.onRejectedCallbacks.forEach((item) => {
            if (typeof item === 'function') {
                item(reason);
            }
        });
    }
}

Promise.PENDING = 'pending';
Promise.FULFILLED = 'fulfilled';
Promise.REJECTED = 'rejected';


module.exports = Promise;
